import {
  View,
  Text,
  Image,
  FlatList,
  StyleSheet,
  Dimensions,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import {Slider} from '@rneui/themed';
import React, {useEffect, useRef, useState} from 'react';
import {useRoute} from '@react-navigation/native';
import {songs} from '../helpers/musicData';
import TrackPlayer, {
  Capability,
  usePlaybackState,
  useProgress,
  State,
} from 'react-native-track-player';
import {
  moderateScale,
  verticalScale,
  horizontalScale,
} from '../helpers/sizeHelpers';
const {height, width} = Dimensions.get('window');

const MusicScreen = () => {
  const route = useRoute();
  const progress = useProgress();
  const {state:playbackState} = usePlaybackState();
  const [currentSong, setCurrentSong] = useState(route.params.index);
  const [sliderValue, setSliderValue] = useState(0);
  const ref = useRef();

  useEffect(() => {
    setTimeout(() => {
      ref.current.scrollToIndex({
        animated: true,
        index: currentSong,
      });
    }, 500);
  }, []);

  useEffect(() => {
    setupPlayer();
  }, []);

  useEffect(() => {
    if (playbackState === State.Playing) {
      const updateInterval = setInterval(async () => {
        const position = await TrackPlayer.getPosition();
        setSliderValue(position);
      }, 1000);

      return () => {
        clearInterval(updateInterval);
      };
    }
  }, [playbackState]);

  const setupPlayer = async () => {
    try {
      await TrackPlayer.setupPlayer();
      await TrackPlayer.updateOptions({
        // Media controls capabilities
        capabilities: [
          Capability.Play,
          Capability.Pause,
          Capability.SkipToNext,
          Capability.SkipToPrevious,
          Capability.Stop,
        ],

        // Capabilities that will show up when the notification is in the compact form on Android
        compactCapabilities: [Capability.Play, Capability.Pause],
        // Icons for the notification on Android (if you don't like the default ones)
      });
      await TrackPlayer.add(songs);
      await TrackPlayer.skip(currentSong);
      togglePlayback(playbackState);
    } catch (e) {
      console.log(e, 'e...');
    }
  };

  const togglePlayback = async playbackState => {
    if (
      playbackState === State.Paused ||
      playbackState === State.Ready 
      // ||playbackState === State.Buffering ||
      // playbackState === State.Connecting
    ) {
      await TrackPlayer.play();
    } else {
      await TrackPlayer.pause();
    }
  };

  console.log(playbackState, '...playbackState');
  return (
    <SafeAreaView style={styles.container}>
      <View>
        <FlatList
          horizontal
          ref={ref}
          showsHorizontalScrollIndicator={false}
          pagingEnabled
          data={songs}
          onScroll={async e => {
            const x = e.nativeEvent.contentOffset.x / width;
            setCurrentSong(parseInt(x.toFixed(0)));
            await TrackPlayer.skip(parseInt(x.toFixed(0)));
            togglePlayback(playbackState);
          }}
          renderItem={({item, index}) => {
            return (
              <View style={styles.bannerView}>
                <Image source={item.artwork} style={styles.banner} />
                <Text style={styles.name}>{item.title}</Text>
                <Text style={styles.name}>{item.artist}</Text>
              </View>
            );
          }}
        />
      </View>

      <View style={styles.sliderView}>
        <Slider
          // value={progress.position}
          value={sliderValue}
          maximumValue={progress.duration}
          minimumValue={0}
          thumbStyle={{width: moderateScale(20), height: moderateScale(20)}}
          thumbTintColor={'black'}
          onValueChange={async value => {
            // await TrackPlayer.seekTo(value);
            setSliderValue(value);
          }}
          onSlidingComplete={async value => {
            await TrackPlayer.pause();

            await TrackPlayer.seekTo(value);

            await TrackPlayer.play();
          }}
        />
      </View>
      <View style={styles.btnArea}>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={async () => {
            if (currentSong > 0) {
              setCurrentSong(currentSong - 1);
              ref.current.scrollToIndex({
                animated: true,
                index: parseInt(currentSong) - 1,
              });
              await TrackPlayer.skip(parseInt(currentSong) - 1);
              togglePlayback(playbackState);
            }
          }}>
          <Image
            source={require('../images/previous.png')}
            style={styles.icon}
          />
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={async () => {
            togglePlayback(playbackState);
          }}>
          <Image
            source={
              playbackState == State.Paused || playbackState == State.Ready
                ? require('../images/play.png')
                : require('../images/pause.png')
            }
            style={[
              styles.icon,
              {width: moderateScale(50), height: moderateScale(50)},
            ]}
          />
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={async () => {
            if (songs.length - 1 > currentSong) {
              setCurrentSong(currentSong + 1);
              ref.current.scrollToIndex({
                animated: true,
                index: parseInt(currentSong) + 1,
              });
              await TrackPlayer.skip(parseInt(currentSong) + 1);
              togglePlayback(playbackState);
            }
          }}>
          <Image source={require('../images/next.png')} style={styles.icon} />
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default MusicScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bannerView: {
    width: width,
    height: height / 2 - 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: moderateScale(10),
    marginTop: verticalScale(50),
  },
  banner: {
    width: '90%',
    height: '100%',
    borderRadius: moderateScale(10),
  },
  name: {
    marginTop: verticalScale(10),
    fontSize: moderateScale(20),
    marginLeft: horizontalScale(20),
    fontWeight: '700',
    color: '#000',
  },
  sliderView: {
    marginTop: verticalScale(20),
    alignSelf: 'center',
    width: '90%',
  },
  btnArea: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    marginTop: verticalScale(50),
  },
  icon: {
    width: horizontalScale(35),
    height: verticalScale(35),
  },
  durationView: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: horizontalScale(20),
    paddingRight: horizontalScale(20),
    marginTop: verticalScale(20),
  },
});
