import {
  View,
  Text,
  StyleSheet,
  FlatList,
  SafeAreaView,
} from 'react-native';
import React from 'react';
import {songs} from '../helpers/musicData';
import MusicListItem from '../components/MusicListItem';
import {
  horizontalScale,
  moderateScale,
  verticalScale,
} from '../helpers/sizeHelpers';

const HomeScreen = () => {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.logo}>Audio Player</Text>
      </View>
      <FlatList
        data={songs}
        renderItem={({item, index}) => {
          return <MusicListItem item={item} index={index} data={songs} />;
        }}
      />
    </SafeAreaView>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    height: verticalScale(60),
    backgroundColor: '#fff',
    width: '100%',
    elevation: 5,
    justifyContent: 'center',
  },
  logo: {
    fontSize: moderateScale(20),
    fontWeight: '700',
    color: 'black',
    marginLeft: horizontalScale(20),
  },
});
