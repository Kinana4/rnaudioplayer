import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Image,
} from 'react-native';
import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {
  horizontalScale,
  moderateScale,
  verticalScale,
} from '../helpers/sizeHelpers';
const {height, width} = Dimensions.get('window');

const MusicListItem = ({item, index, data}) => {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      activeOpacity={0.7}
      style={[
        styles.conatainer,
        {marginBottom: index == data.length - 1 ? 30 : 0},
      ]}
      onPress={() => {
        navigation.navigate('MusicScreen', {
          data: item,
          index: index,
        });
      }}>
      <Image source={item.artwork} style={styles.songImage} />
      <View style={styles.nameView}>
        <Text style={styles.name}>{item.title}</Text>
        <Text style={styles.artistName}>{item.artist}</Text>
      </View>
      <View style={{padding: moderateScale(10)}}>
        <Image source={require('../images/play.png')} style={styles.play} />
      </View>
    </TouchableOpacity>
  );
};

export default MusicListItem;

const styles = StyleSheet.create({
  conatainer: {
    width: width - 20,
    height: verticalScale(100),
    elevation: 5,
    marginTop: verticalScale(20),
    alignSelf: 'center',
    backgroundColor: '#fff',
    borderRadius: moderateScale(10),
    flexDirection: 'row',
    alignItems: 'center',
  },
  songImage: {
    width: horizontalScale(100),
    height: verticalScale(90),
    borderRadius: moderateScale(10),
    marginLeft: horizontalScale(7),
  },
  nameView: {
    paddingLeft: horizontalScale(15),
    width: '50%',
  },
  name: {
    fontSize: moderateScale(17),
    fontWeight: '600',
    color: '#000',
  },
  artistName: {
    fontSize: moderateScale(12),
    marginTop: verticalScale(10),
    color: '#000',
  },
  play: {
    width: moderateScale(30),
    height: moderateScale(30),
    borderRadius: moderateScale(15),
  },
});
