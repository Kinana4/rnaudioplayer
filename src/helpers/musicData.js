export const songs = [
  {
    id: '1',
    title: 'Song with URL',
    artist: 'feat.kinana',
    artwork: require('../images/banner4.jpg'),
    url: 'https://commondatastorage.googleapis.com/codeskulptor-demos/DDR_assets/Kangaroo_MusiQue_-_The_Neverwritten_Role_Playing_Game.mp3',
  },
  {
    id: '2',
    title: 'Song from local',
    artist: 'feat.kinana',
    artwork: require('../images/banner1.jpg'),
    url: require('../music/music1.mp3'),
  },
  {
    id: '3',
    title: 'Song with streaming',
    artist: 'feat.kinana',
    artwork: require('../images/banner8.jpg'),
    url: 'http://qthttp.apple.com.edgesuite.net/1010qwoeiuryfg/sl.m3u8',
    type: 'hls',
  },
];
